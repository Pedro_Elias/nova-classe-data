package poo_upis;

public interface IData {
	boolean ehBissexto(int ano);
	byte getUltimoDia(int mes, int ano);
	
	int qualDia(int dia);
	int qualMes(int dia);
	
	byte getDia();
	void setDia(byte dia);
	
	byte getMes();
	void setMes(byte mes);
	
	short getAno();
	void setAno(short ano);
	
	void incrementaDia();
	void incrementaMes();
	void incrementaAno();
	
	void incrementaNDias(int numeroD);
	void incrementaNMes(int numeroM);
	void incrementaNAno(int numeroA);
	
	void print();
}

